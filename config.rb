require 'compass/import-once/activate'
require 'autoprefixer-rails'

# Require any additional compass plugins here.

# Set this to the root of your project when deployed:
Encoding.default_external = 'utf-8'
http_path = '/dist_msass'
# css_dir = '/temp/dist/css'
css_dir = '/dist_msass/css'
sass_dir = '/msass/sass'
images_dir = '/dist_msas/images'
javascripts_dir = 'dist_msas/js'
sourcemap     = true
# output_style  = :compressed
sass_options  = { cache: true }
line_comments = false


on_stylesheet_saved do |file|
    css = File.read(file)
    map = file + '.map'

    if File.exists? map
        result = AutoprefixerRails.process(css,
                                           from: file,
                                           to:   file,
                                           map:  { prev: File.read(map), inline: false },
                                           cascade: true,
                                           browsers:  ['> 1%', 'last 2 versions', 'ie >= 10', 'Android >= 2.4', 'Safari >= 5']
        )
        File.open(file, 'w') { |io| io << result.css }
        File.open(map,  'w') { |io| io << result.map }
    else
        File.open(file, 'w') { |io| io << AutoprefixerRails.process(css) }
    end
end
